require 'net/http'
require 'json'

class TweetsController < ApplicationController

	# usuário da locaweb no twitter tem o ID 42
	LOCAWEB_ID = 42
	USERNAME = "tseiiti@gmail.com"

  def index
  	# busca tweeps no sistema simplificado da locaweb
		uri = URI('http://xxxcnn0330.locaweb.com.br/tweeps')
		http = Net::HTTP.new(uri.host, uri.port)
		get = Net::HTTP::Get.new(uri.path)
		get['Username'] = USERNAME
		httpok = http.request(get)
		hash = JSON.parse(httpok.body)

		@mentioned_tweets = []
		hash['statuses'].each do |tweet|
			user_mentions = tweet['entities']['user_mentions']

			# buscar tweets que mencionem o usuário da Locaweb
			if tweet['in_reply_to_user_id'] != LOCAWEB_ID
				user_mentions.each do |mention|

					# excluir tweets que sejam replies para tweets da locaweb
					if mention['id'] == LOCAWEB_ID
						@mentioned_tweets << tweet
					end
				end
			end
		end

		# dar prioridade aos usuários com mais seguidores,
		# tweets tenham mais retweets
		# que o tweet seja favoritado mais vezes
		@mentioned_tweets.sort_by! do |tweet|
			[-tweet['user']['followers_count'], -tweet['retweet_count'], -tweet['favorite_count']]
		end
  end
end
